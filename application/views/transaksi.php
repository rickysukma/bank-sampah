<script type="text/javascript">
  calculate = function()
  {
    var harga = document.getElementById('harga').value;
    var jumlah = document.getElementById('jumlah').value; 
    document.getElementById('total').value = parseInt(harga)*parseInt(jumlah);

   }
</script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admin">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Transaksi</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Transaksi</h1>
      </div>
    </div><!--/.row-->
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('notif')){
          $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Success!</strong> $pesan.
  </div>";
        } elseif($this->session->flashdata('notif')) {
            $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Gagal!</strong> $pesan.
  </div>";
        }
          ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --><button style="float: right" data-toggle="modal" data-target="#tambah-data" class="btn btn-primary btn-md" id="btn-todo">Tambah</button></div>
          <div class="panel-body">
            <!-- Table Harga -->
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tanggal Transaksi</th>
                  <th>Sampah</th>
                  <th>Jumlah</th>
                  <th>Total</th>
                  <th>Nomer Rekening </th>
                  <th>Tipe Transaksi</th>
                  <!-- <th>Aksi</th> -->
                </tr>
              </thead>
              <tbody>
                <?php $no=0; foreach ($transaksi as $row){ 
                $tgl_transaksi = $row['tgl_transaksi'];
                $jumlah = $row['jumlah'];
                $total = $row['total'];
                $norek_akun = $row['norek_akun'];
                // $nama = $row['nama'];
                // $alamat = $row['alamat'];
                // $no_telp = $row['no_telp'];
                // $pin = $row['pin'];
                ?>
                <tr>
                  <td><?php echo ++$no ?></td>
                  <td><?php echo $row['tgl_transaksi'] ?></td>
                  <td><?php echo $row['nama_sampah'] ?></td>
                  <td><?php echo $row['jumlah'] ?></td>
                  <td><?php echo $row['total'] ?></td>
                  <td><?php echo $row['norek_akun'] ?></td>
                  <td>Deposit</td>
                  <!-- <td><a href="<?php $id = $row['id_transaksi']; echo(base_url('admin/nasabah_detail/'.base64_encode($id))) ?>" class="btn btn-xs btn-success">Detail</a> <a data-toggle="modal" data-target="#edit-data<?php echo $id ?>" class="btn btn-xs btn-info">Ubah</a>
            <a data-toggle="modal" data-target="#hapus-data<?php echo $id ?>"  id="btn-todo" class="btn btn-xs btn-danger">Hapus</a></td> -->
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <!--  -->
          </div>
           <!-- HAPUS MODAL -->
           <?php
        foreach($nasabah as $i):
            $id_nasabah=$i['id_nasabah'];
            $nama_nasabah=$i['nama'];
            // $alamat_nasabah=$i['alamat'];
            // $no_nasabah=$i['no_telp'];
            // $pin_nasabah=$i['pin'];
        ?>

      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="hapus-data<?php echo $id_nasabah ?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Perhatian!</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/del_nasabah/'.$id_nasabah)?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <p align="center">Apakah yakin ingin menghapus data transaksi bernama <b><?php echo $nama_nasabah; ?></b> ini?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit"> Hapus&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
      <?php endforeach ?>
      <!-- END HAPUS MODAL -->
      </div><!--/.col-->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-data" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Tambah Transaksi</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('transaksi/tambah_transaksi')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">No Rekekning</label>
                            <div class="col-lg-10">
                                <input list="no_rek" class="form-control" required="" name="norek_akun" placeholder="Tuliskan no Rekening">
                                <datalist id="no_rek">
                                  <?php foreach ($akun as $ak) {
                                    echo "<option class='harga-sampah' value='".$ak['norek_akun']."''>";echo $ak['norek_akun'];
                                } ?>
                                </datalist>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Sampah</label>
                            <div class="col-lg-10">
                              <input list="sampah" class="form-control" name="sampah" required="" placeholder="Sampah">
                              <datalist id="sampah">
                                  <?php foreach ($sampah as $sa) {
                                    echo "<option id='$sa[harga_sampah]' value='".$sa['nama_sampah']."''>";echo $sa['nama_sampah'];
                                } ?>
                                </datalist>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Harga</label>
                            <div class="col-lg-10">
                                <input type="number" class="form-control" id="harga" required="" name="harga" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Jumlah</label>
                            <div class="col-lg-10">
                                <input type="text" maxlength="6" required="" onblur="calculate()"  class="form-control" name="jumlah" id="jumlah" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Total</label>
                            <div class="col-lg-10">
                                <input type="text" id="total" class="form-control" name="total" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div> 
      <!-- MODAL TAMBAH END -->
      <!-- END -->


  </div>