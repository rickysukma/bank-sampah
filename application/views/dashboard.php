<script type="text/javascript">
  calculate = function()
  {
    var harga = document.getElementById('harga').value;
    var jumlah = document.getElementById('jumlah').value; 
    document.getElementById('total').value = parseInt(harga)*parseInt(jumlah);

   }
</script>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>admin">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Dashboard</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Dashboard</h1>
      </div>
    </div><!--/.row-->

    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-default">
          <?php if($this->session->flashdata('notif')){
          $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Success!</strong> $pesan.
  </div>";
        } elseif($this->session->flashdata('notif')) {
            $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Gagal!</strong> $pesan.
  </div>";
        }
          ?>
          <div class="panel-heading">
            Harga Beli Sampah
            <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --></div>
          <div class="panel-body">
            <!-- Table Harga -->
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Jenis Sampah</th>
                  <th>Nama Sampah</th>
                  <th>Harga/Kg</th>
                </tr>
              </thead>
              <tbody>
                <?
                    foreach ($sampah as $row) {
                      $nama_sampah = $row['nama_sampah'];
                      $tipe_sampah = $row['tipe_sampah'];
                      $harga_sampah = $row['harga_sampah'];
                ?>
                <tr>
                  <td><? echo $tipe_sampah; ?></td>
                  <td><? echo $nama_sampah; ?></td>
                  <td><? echo money_format("%.0n" ,$harga_sampah); }?></td>
                </tr>
              </tbody>
            </table>
            <!--  -->
          </div>
          <div class="panel-footer">
            <div class="input-group">
              <!-- <input id="btn-input" type="text" class="form-control input-md" placeholder="Add new task" /><span class="input-group-btn"> -->
                <button data-toggle="modal" data-target="#tambah-sampah" class="btn btn-primary btn-md" id="btn-todo">Tambah</button>
            </span></div>
          </div>
        </div>
      </div><!--/.col-->
      <div class="col-md-6">
        <div class="panel panel-default">
        <form class="form-horizontal" action="<?php echo base_url('transaksi/tambah_transaksi')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">No Rekekning</label>
                            <div class="col-lg-10">
                                <input list="no_rek" class="form-control" required="" name="norek_akun" placeholder="Tuliskan no Rekening">
                                <datalist id="no_rek">
                                  <?php foreach ($akun as $ak) {
                                    echo "<option class='harga-sampah' value='".$ak['norek_akun']."''>";echo $ak['norek_akun'];
                                } ?>
                                </datalist>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Sampah</label>
                            <div class="col-lg-10">
                              <input list="sampah" class="form-control" name="sampah" required="" placeholder="Sampah">
                              <datalist id="sampah">
                                  <?php foreach ($sampah as $sa) {
                                    echo "<option id='$sa[harga_sampah]' value='".$sa['nama_sampah']."''>";echo $sa['nama_sampah'];
                                } ?>
                                </datalist>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Harga</label>
                            <div class="col-lg-10">
                                <input type="number" class="form-control" id="harga" required="" name="harga" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Jumlah</label>
                            <div class="col-lg-10">
                                <input type="text" maxlength="6" required="" onblur="calculate()"  class="form-control" name="jumlah" id="jumlah" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Total</label>
                            <div class="col-lg-10">
                                <input type="text" id="total" class="form-control" name="total" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
                </div>
              </div>
              <!--  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-sampah" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Tambah Data Sampah</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/add_sampah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama Sampah</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="nama_sampah" placeholder="Tuliskan Nama Sampah">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Tipe Sampah</label>
                            <div class="col-lg-10">
                              <select name="tipe_sampah">
                                <option value="Logam">Logam</option>
                                <option value="Non Logam">Non Logam</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Harga Sampah</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="harga_sampah" placeholder="Tuliskan Harga Sampah">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>