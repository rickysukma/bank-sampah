
      <!-- end modal hapus  -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>admin">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Data Sampah</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Data Sampah</h1>
      </div>
    </div><!--/.row-->

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <?php if($this->session->flashdata('notif')){
          $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Success!</strong> $pesan.
  </div>";
        } elseif($this->session->flashdata('notif')) {
            $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Gagal!</strong> $pesan.
  </div>";
        }
          ?>
          <div class="panel-heading">
            Harga Beli Sampah
            <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --></div>
          <div class="panel-body">
            <!-- Table Harga -->
            <table class="table table-hover" >
              <thead>
                <tr class="table table-succes">
                  <th>Jenis Sampah</th>
                  <th>Nama Sampah</th>
                  <th>Harga/Kg</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?
                    foreach ($sampah as $row) {
                    	$id=$row['id_sampah'];
                      $nama_sampah = $row['nama_sampah'];
                      $tipe_sampah = $row['tipe_sampah'];
                      $harga_sampah = $row['harga_sampah'];
                ?>
                <tr>
                  <td><? echo $tipe_sampah; ?></td>
                  <td><? echo $nama_sampah; ?></td>
                  <td><? echo money_format("%.0n" ,$harga_sampah); ?></td>
                  <td><a data-toggle="modal" data-target="#edit-data<?php echo $id ?>" class="btn btn-xs btn-info">Ubah</a>
            <a data-toggle="modal" data-target="#hapus-data<?php echo $id ?>"  id="btn-todo" class="btn btn-xs btn-danger">Hapus</a></td>
                </tr>
            <?php } ?>
              </tbody>
            </table>
            <!--  -->
          </div>
          <div class="panel-footer">
            <div class="input-group">
              <!-- <input id="btn-input" type="text" class="form-control input-md" placeholder="Add new task" /><span class="input-group-btn"> -->
                <button data-toggle="modal" data-target="#tambah-sampah" class="btn btn-primary btn-md" id="btn-todo">Tambah</button>
            </span></div>
          </div>
        </div>
      </div><!--/.col-->


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-sampah" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Tambah Data Sampah</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/add_sampah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama Sampah</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="nama_sampah" placeholder="Tuliskan Nama Sampah">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Tipe Sampah</label>
                            <div class="col-lg-10">
                              <select name="tipe_sampah">
                                <option value="Logam">Logam</option>
                                <option value="Non Logam">Non Logam</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Harga Sampah</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="harga_sampah" placeholder="Tuliskan Harga Sampah">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>

      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-sampah" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Tambah Data</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/add_nasabah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">No Rek.</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" readonly="" name="norek" value="<?php echo $norek ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="nama" placeholder="Tuliskan Nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-lg-10">
                              <textarea class="form-control" name="alamat" required="" placeholder="Tuliskan Alamat"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nomer Telepon</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="no_telp" placeholder="Tuliskan No Telepon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">PIN</label>
                            <div class="col-lg-10">
                                <input type="password" maxlength="6" required=""  class="form-control" name="pin" placeholder="Tuliskan PIN">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div> 
      
      <?php
        foreach($sampah as $i):
            $id_sampah=$i['id_sampah'];
            $nama_sampah=$i['nama_sampah'];
            $tipe_sampah = $i['tipe_sampah'];
            $harga_sampah = $i['harga_sampah'];
            // $alamat_nasabah=$i['alamat'];
            // $no_nasabah=$i['no_telp'];
            // $pin_nasabah=$i['pin'];
        ?>

      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="hapus-data<?php echo $id_sampah ?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Perhatian!</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/del_sampah/'.$id_sampah)?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <p align="center">Apakah yakin ingin menghapus data bernama <b><?php echo $nama_sampah; ?></b> ini?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit"> Hapus&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
      <?php endforeach ?>

       <!-- MODAL EDIT -->
      <?php
        foreach($sampah as $i):
             $id_sampah=$i['id_sampah'];
            $nama_sampah=$i['nama_sampah'];
            $tipe_sampah = $i['tipe_sampah'];
            $harga_sampah = $i['harga_sampah'];
        ?>
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data<?php echo $id_sampah ?>" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Update Data Sampah</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url('admin/update_sampah/'.$id_sampah);?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                       <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-sampah" 	class="modal fade">
						</div>
                </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama Sampah</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" value="<?php echo $nama_sampah ?>" name="nama_sampah" placeholder="Tuliskan Nama Sampah">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Tipe Sampah</label>
                            <div class="col-lg-10">
                              <select name="tipe_sampah">
                              	<?php 
                              	if ($tipe_sampah == 'Logam') {
                              	 
                              	 echo	'<option selected="" value="Logam">Logam</option>';
                                echo '<option value="Non Logam">Non Logam</option>';
                              	 }else{
                              	 	$select = 'selected=""';
                              	 echo	'<option value="Logam">Logam</option>';
                                echo '<option selected="" value="Non Logam">Non Logam</option>';
                              	 } ?>
                                <option value="Logam">Logam</option>
                                <option value="Non Logam">Non Logam</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Harga Sampah</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" value="<?php echo $harga_sampah ?>"	 required="" name="harga_sampah" placeholder="Tuliskan Harga Sampah">
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                </div>
             </form>
        </div>
          </div>
      </div> 
      <!-- END -->
<? endforeach?>
