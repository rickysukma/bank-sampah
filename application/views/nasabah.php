<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="#">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Nasabah</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Nasabah</h1>
      </div>
    </div><!--/.row-->
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('notif')){
          $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Success!</strong> $pesan.
  </div>";
        } elseif($this->session->flashdata('notif')) {
            $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Gagal!</strong> $pesan.
  </div>";
        }
          ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            Data Nasabah
            <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --><button style="float: right" data-toggle="modal" data-target="#tambah-data" class="btn btn-primary btn-md" id="btn-todo">Tambah</button></div>
          <div class="panel-body">
            <!-- Table Harga -->
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Nomer Telepon</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=0; foreach ($data as $row){ ?>
                <tr>
                  <td><?php echo ++$no ?></td>
                  <td><?php echo $row['nama'] ?></td>
                  <td><?php echo $row['alamat'] ?></td>
                  <td><?php echo $row['no_telp'] ?></td>
                  <td><a href="#" class="btn btn-xs btn-info">Ubah</a>
            <a data-toggle="modal" data-target="#hapus-data"  id="btn-todo" class="btn btn-xs btn-danger">Hapus</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <!--  -->
          </div>
          <div class="panel-footer">
            <div class="input-group">
              <!-- <input id="btn-input" type="text" class="form-control input-md" placeholder="Add new task" /><span class="input-group-btn"> -->
                <!-- <button class="btn btn-primary btn-md" id="btn-todo">Tambah</button> -->
                <!-- RENCANA PAGINATION -->
            </span></div>
          </div>
        </div>
      </div><!--/.col-->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-data" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Tambah Data</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/add_nasabah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">NO Rekening</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="norek" placeholder="" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="nama" placeholder="Tuliskan Nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-lg-10">
                              <textarea class="form-control" name="alamat" required="" placeholder="Tuliskan Alamat"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nomer Telepon</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="no_telp" placeholder="Tuliskan No Telepon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">PIN</label>
                            <div class="col-lg-10">
                                <input type="password" maxlength="6" required=""  class="form-control" name="pin" placeholder="Tuliskan PIN">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Ulangi PIN</label>
                            <div class="col-lg-10">
                                <input type="password" maxlength="6" required="" class="form-control" name="pin" placeholder="Tuliskan ulang PIN">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div> 
      <!-- MODAL TAMBAH END -->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="hapus-data" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Perhatian!</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/add_nasabah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <p align="center">Apakah yakin ingin menghapus data ini?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit"> Hapus&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
