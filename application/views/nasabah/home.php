<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#datatables').DataTable();
} );
</script>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admin">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Nasabah</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Nasabah</h1>
      </div>
    </div><!--/.row-->
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('notif')){
          $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Success!</strong> $pesan.
  </div>";
        } elseif($this->session->flashdata('gagal')) {
            $pesan = $this->session->flashdata('gagal');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Gagal!</strong> $pesan.
  </div>";
        }
          ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            Data Nasabah
            <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --><button style="float: right" data-toggle="modal" data-target="#tambah-data" class="btn btn-primary btn-md" id="btn-todo">Tambah</button></div>
          <div class="panel-body">
            <!-- Table Harga -->
            <table class="table table-bordered" id="datatables">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Nomer Telepon</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=0; foreach ($nasabah as $row){ 
                $nama = $row['nama'];
                $alamat = $row['alamat'];
                $no_telp = $row['no_telp'];
                // $pin = $row['pin'];
                ?>
                <tr>
                  <td><?php echo ++$no ?></td>
                  <td><?php echo $row['nama'] ?></td>
                  <td><?php echo $row['alamat'] ?></td>
                  <td><?php echo $row['no_telp'] ?></td>
                  <td><a href="<?php $id = $row['id_nasabah']; echo(base_url('admin/nasabah_detail/'.base64_encode($id))) ?>" class="btn btn-xs btn-success">Detail</a> <a data-toggle="modal" data-target="#edit-data<?php echo $id ?>" class="btn btn-xs btn-info">Ubah</a>
            <a data-toggle="modal" data-target="#hapus-data<?php echo $id ?>"  id="btn-todo" class="btn btn-xs btn-danger">Hapus</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <!--  -->
          </div>
           <!-- HAPUS MODAL -->
           <?php
        foreach($nasabah as $i):
            $id_nasabah=$i['id_nasabah'];
            $nama_nasabah=$i['nama'];
            $id_rek = $i['norek_akun']
            // $alamat_nasabah=$i['alamat'];
            // $no_nasabah=$i['no_telp'];
            // $pin_nasabah=$i['pin'];
        ?>

      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="hapus-data<?php echo $id_nasabah ?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Perhatian!</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/del_nasabah/'.$id_rek)?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <p align="center">Apakah yakin ingin menghapus data bernama <b><?php echo $nama_nasabah; ?></b> ini?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit"> Hapus&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
      <?php endforeach ?>
      <!-- END HAPUS MODAL -->
      </div><!--/.col-->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-data" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Tambah Data</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/add_nasabah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">No Rek.</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" readonly="" name="norek" value="<?php echo $norek ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="nama" placeholder="Tuliskan Nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-lg-10">
                              <textarea class="form-control" name="alamat" required="" placeholder="Tuliskan Alamat"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nomer Telepon</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="no_telp" placeholder="Tuliskan No Telepon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">PIN</label>
                            <div class="col-lg-10">
                                <input type="password" maxlength="6" required=""  class="form-control" name="pin" placeholder="Tuliskan PIN">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div> 
      <!-- MODAL TAMBAH END -->

      <!-- MODAL EDIT -->
      <?php
        foreach($nasabah as $i):
            $id_nasabah=$i['id_nasabah'];
            $nama_nasabah=$i['nama'];
            $alamat_nasabah=$i['alamat'];
            $no_nasabah=$i['no_telp'];
            $pin_nasabah=$i['pin'];
            $rekening=$i['norek_akun'];
        ?>
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data<?php echo $id_nasabah ?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Edit Data</h4>
              </div>

              <form class="form-horizontal" action="<?php echo base_url('admin/update_nasabah/'.$id_nasabah);?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">No Rekening</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control"  readonly="" required="" name="norek" placeholder="Tuliskan Nama" value="<?php echo $rekening ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" value="<?php echo $nama_nasabah ?>" required="" name="nama" placeholder="Tuliskan Nama" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-lg-10">
                              <textarea class="form-control" name="alamat" required="" placeholder="Tuliskan Alamat"><?php echo $alamat_nasabah ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nomer Telepon</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" value="<?php echo $no_nasabah ?>" name="no_telp" placeholder="Tuliskan No Telepon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">PIN</label>
                            <div class="col-lg-10">
                                <input type="password" readonly="" value="<?php echo $pin_nasabah ?>" maxlength="6" required=""  class="form-control" name="pin" placeholder="Tuliskan PIN">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div> 
      <!-- END -->
<? endforeach?>
  </div>