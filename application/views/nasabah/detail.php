<?php
	foreach ($data as $row) {
		echo $row['nama'];
	}
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="#">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Nasabah</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Nasabah</h1>
      </div>
    </div><!--/.row-->
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('notif')){
          $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Success!</strong> $pesan.
  </div>";
        } elseif($this->session->flashdata('notif')) {
            $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Gagal!</strong> $pesan.
  </div>";
        }
          ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            Data Nasabah
            <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --><!-- <button style="float: right" data-toggle="modal" data-target="#tambah-data" class="btn btn-primary btn-md" id="btn-todo">Tambah</button> --></div>
          <div class="panel-body">
            <!-- Table Harga -->
            <div class="col-md-5">
            <table class="table table-striped">
              <tbody>
                <?php $no=0; foreach ($data as $row){ 
                $nama = $row['nama'];
                $alamat = $row['alamat'];
                $no_telp = $row['no_telp'];
                $pin = $row['pin'];
                ?>
                <tr>
                  <td>Nama</td>
                  <td><?php echo $row['nama'] ?></td>
                </tr>
                 <tr>
                  <td>Alamat</td>
                  <td><?php echo $row['alamat'] ?></td>
                </tr>
                 <tr>
                  <td>No.Telephone</td>
                  <td><?php echo $row['no_telp'] ?></td>
                </tr>
                <tr>
                  <td>No.Rekening</td>
                  <td><?php echo $row['norek_akun'] ?></td>
                </tr>
                <tr>
                  <td>Saldo</td>
                  <td><?php echo $row['saldo'] ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            </div>
            <!--  -->
          </div>
           <!-- HAPUS MODAL -->
           <?php
        foreach($data as $i):
            $id_nasabah=$i['id_nasabah'];
            $nama_nasabah=$i['nama'];
            // $alamat_nasabah=$i['alamat'];
            // $no_nasabah=$i['no_telp'];
            // $pin_nasabah=$i['pin'];
        ?>

      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="hapus-data<?php echo $id_nasabah ?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Perhatian!</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/del_nasabah/'.$id_nasabah)?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <p align="center">Apakah yakin ingin menghapus data bernama <b><?php echo $nama_nasabah; ?></b> ini?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit"> Hapus&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
      <?php endforeach ?>
      <!-- END HAPUS MODAL -->
      </div><!--/.col-->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-data" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Tambah Data</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('admin/add_nasabah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="nama" placeholder="Tuliskan Nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-lg-10">
                              <textarea class="form-control" name="alamat" required="" placeholder="Tuliskan Alamat"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nomer Telepon</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" name="no_telp" placeholder="Tuliskan No Telepon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">PIN</label>
                            <div class="col-lg-10">
                                <input type="password" maxlength="6" required=""  class="form-control" name="pin" placeholder="Tuliskan PIN">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Ulangi PIN</label>
                            <div class="col-lg-10">
                                <input type="password" maxlength="6" required="" class="form-control" name="pin" placeholder="Tuliskan ulang PIN">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div> 
      <!-- MODAL TAMBAH END -->

      <!-- MODAL EDIT -->
      <?php
        foreach($data as $i):
            $id_nasabah=$i['id_nasabah'];
            $nama_nasabah=$i['nama'];
            $alamat_nasabah=$i['alamat'];
            $no_nasabah=$i['no_telp'];
            $pin_nasabah=$i['pin'];
        ?>
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data<?php echo $id_nasabah ?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Edit Data</h4>
              </div>

              <form class="form-horizontal" action="<?php echo base_url('admin/update_nasabah/'.$id_nasabah);?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" value="<?php echo $nama_nasabah ?>" required="" name="nama" placeholder="Tuliskan Nama" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-lg-10">
                              <textarea class="form-control" name="alamat" required="" placeholder="Tuliskan Alamat"><?php echo $alamat_nasabah ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nomer Telepon</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" value="<?php echo $no_nasabah ?>" name="no_telp" placeholder="Tuliskan No Telepon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">PIN</label>
                            <div class="col-lg-10">
                                <input type="password" readonly="" value="<?php echo $pin_nasabah ?>" maxlength="6" required=""  class="form-control" name="pin" placeholder="Tuliskan PIN">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div> 
      <!-- END -->
<? endforeach?>

  </div>