<?php 
  if (site_url('admin')) {
    $nasabah = "active";
    $dashboard = "";
  } else {
    $nasabah = "";
    $dashboard = "active";
  }
  
?>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span></button>
        <a class="navbar-brand" href="#"><span>Bank Sampah</span>&nbsp; - Admin</a>
      </div>
    </div><!-- /.container-fluid -->
  </nav>
  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
      <div class="profile-userpic">
        <img src="<?php echo base_url('/assets/img/bank-sampah.png') ?>" class="img-responsive" alt="">
      </div>
      <div class="profile-usertitle">
        <div class="profile-usertitle-name"><p style="text-transform: uppercase;"><?php echo $this->session->userdata('nama') ?></p></div>
        <div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="divider"></div>
    <ul id="nav" class="nav menu">
      <li><a href="<?php echo base_url('admin')?>"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
      <li><a href="<?php echo base_url('admin/nasabah') ?>"><em class="fa fa-users">&nbsp;</em> Nasabah</a></li>
      <li><a href="<?php echo base_url('akun') ?>"><em class="fa fa-book">&nbsp;</em> Account Bank</a></li>
      <li><a href="<?php echo base_url('transaksi') ?>"><em class="fa fa-toggle-off">&nbsp;</em> Transaksi</a></li>
      <li><a href="<?php echo base_url('c_sampah') ?>"><em class="fa fa-clone">&nbsp;</em> Sampah</a></li>
      <li><a href="<?php echo base_url('login/logout') ?>"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
    </ul>
  </div>