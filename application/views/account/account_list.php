<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admin">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Account Bank</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Account</h1>
      </div>
    </div><!--/.row-->
    <div class="row">
      <div class="col-md-12">
        <?php if($this->session->flashdata('notif')){
          $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Success!</strong> $pesan.
  </div>";
        } elseif($this->session->flashdata('notif')) {
            $pesan = $this->session->flashdata('notif');
          echo "<div class='alert alert-success alert-dismissible'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>Gagal!</strong> $pesan.
  </div>";
        }
          ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            Account Bank Data
            <!-- <span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span> --><!-- <button style="float: right" data-toggle="modal" data-target="#tambah-data" class="btn btn-primary btn-md" id="btn-todo">Tambah</button> --></div>
          <div class="panel-body">
            <!-- Table Harga -->
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>No. Account Bank</th>
                  <th>Tanggal Aktif</th>
                  <th>Saldo</th>
                  <!-- <th>Nomer Telepon</th> -->
                  <!-- <th>Aksi</th> -->
                </tr>
              </thead>
              <tbody>
                <?php $no=0; foreach ($data as $row){ 
                $norek_akun = $row['norek_akun'];
                $data_opened = $row['date_opened'];
                $balance = $row['balance'];
                ?>
                <tr>
                  <td><?php echo ++$no ?></td>
                  <td><?php echo $norek_akun ?></td>
                  <td><?php echo $data_opened ?></td>
                  <td><?php echo $balance ?></td>
                  <!-- <td> -->
                  	<!-- <a href="<?php $id = $row['norek_akun']; echo(base_url('admin/nasabah_detail/'.base64_encode($id))) ?>" class="btn btn-xs btn-success">Detail</a> --> <!-- <a data-toggle="modal" data-target="#edit-data<?php echo $id ?>" class="btn btn-xs btn-info">Ubah</a> -->
            		<!-- <a data-toggle="modal" data-target="#hapus-data<?php echo $id ?>"  id="btn-todo" class="btn btn-xs btn-danger">Hapus</a> -->
            	<!-- </td> -->
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <!--  -->
          </div>
      </div><!--/.col-->
      <!-- END -->
  </div>