<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Account_m extends CI_Model{
    public function get_account(){
        $this->db->from('akun');
        $this->db->order_by('id_akun','desc');
        $res=$this->db->get(); // Kode ini berfungsi untuk memilih tabel yang akan ditampilkan
        return $res->result_array(); // Kode ini digunakan untuk mengembalikan hasil operasi $res menjadi sebuah array
    }
 
    public function tambah($data){
        $res = $this->db->insert('akun', $data); // Kode ini digunakan untuk memasukan record baru kedalam sebuah tabel
        return $res; // Kode ini digunakan untuk mengembalikan hasil $res
    }
 
    public function update($data, $where){
        $this->db->where('norek_akun',$where);
        $res = $this->db->update('akun',$data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $res;
    }

    public function add_saldo($id,$saldo){
        $this->db->set('balance', 'balance +'.$saldo,FALSE);
        $this->db->where('norek_akun',$id);
        $res = $this->db->update('akun');
        return $res;
    }
 
    public function hapus($where){
        $this->db->where('norek_akun',$where);
        $res = $this->db->delete('akun'); // Kode ini digunakan untuk menghapus record yang sudah ada

        return;
    }

    public function detail($whereid){
        $this->db->from('akun');
        $this->db->where('norek_akun',$whereid);
        $res=$this->db->get(); // Kode ini berfungsi untuk memilih tabel yang akan ditampilkan
        return $res->result_array(); // Kode ini digunakan untuk mengembalikan hasil operasi $res menjadi sebuah array

    }
}
?>