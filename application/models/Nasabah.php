<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Nasabah extends CI_Model{

    public function getData($table, $key, $value) {
        $this->db->order_by($key, $value);
        return $this->db->get($table);
    }

    function nasabah_join_akun($id)
    {
        $this->db->select('*, akun.balance AS saldo');
        $this->db->from('nasabah')->order_by('id_nasabah','desc');
        $this->db->where('id_nasabah', $id);
        $this->db->join('akun', 'akun.norek_akun = nasabah.norek_akun');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function nasabah(){
        $this->db->from('nasabah');
        $this->db->order_by('id_nasabah','desc');
        $res=$this->db->get(); // Kode ini berfungsi untuk memilih tabel yang akan ditampilkan
        return $res->result_array(); // Kode ini digunakan untuk mengembalikan hasil operasi $res menjadi sebuah array
    }
 
    public function tambah($data){
        $res = $this->db->insert('nasabah', $data); // Kode ini digunakan untuk memasukan record baru kedalam sebuah tabel
        return $res; // Kode ini digunakan untuk mengembalikan hasil $res
    }
 
    public function update($data, $where){
        $this->db->where('id_nasabah',$where);
        $res = $this->db->update('nasabah',$data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $res;
    }
 
    public function hapus($where){
        $this->db->where('norek_akun',$where);
        $res = $this->db->delete('nasabah'); // Kode ini digunakan untuk menghapus record yang sudah ada

        return;
    }

    public function detail($whereid){
        $this->db->from('nasabah');
        $this->db->where('id_nasabah',$whereid);
        $res=$this->db->get(); // Kode ini berfungsi untuk memilih tabel yang akan ditampilkan
        return $res->result_array(); // Kode ini digunakan untuk mengembalikan hasil operasi $res menjadi sebuah array

    }
}
?>