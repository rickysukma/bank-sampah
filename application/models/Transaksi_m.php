<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Transaksi_m extends CI_Model{
    public function transaksi(){
        $this->db->from('transaksi');
        $this->db->order_by('id_transaksi','desc');
        $res=$this->db->get(); // Kode ini berfungsi untuk memilih tabel yang akan ditampilkan
        return $res->result_array(); // Kode ini digunakan untuk mengembalikan hasil operasi $res menjadi sebuah array
        // $this->db->select ( '*' ); 
        // $this->db->from ( 'transaksi' );
        // $this->db->join ( 'akun', 'nasabah.cat_id = transaksi.norek_akun' , 'left' );
        // $this->db->join ( 'Soundtrack', 'Soundtrack.album_id = Album.album_id' , 'left' );
        // $query = $this->db->get ();
        // return $query->result ();
    }
 
    public function tambah($data){
        $res = $this->db->insert('transaksi', $data); // Kode ini digunakan untuk memasukan record baru kedalam sebuah tabel
        return $res; // Kode ini digunakan untuk mengembalikan hasil $res
    }
 
    public function update($data, $where){
        $this->db->where('id_sampah',$where);
        $res = $this->db->update('sampah',$data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $res;
    }
 
    public function hapus($where){
        $this->db->where('id_sampah',$where);
        $res = $this->db->delete('sampah'); // Kode ini digunakan untuk menghapus record yang sudah ada

        return;
    }

}
?>