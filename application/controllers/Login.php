<?php
class Login extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('data_login');
    }
 
    function index(){
      if ($this->session->userdata('status') == 'login') {
        redirect(base_url('admin'));
      } else {
        redirect(base_url());
      }
    }

  function salah(){
    $this->session->set_flashdata('salah','Username atau Password Salah');
    $this->load->view('v_login');
  }

 function cek_login(){
  $username = $this->input->post('username');
  $password = $this->input->post('password');
  if ($username== NULL && $password == NULL ) {
    $this->session->set_flashdata('salah','Isikan Form Dengan Benar!!!');
    $this->load->view('v_login');
  }
  $where = array(
   'username' => $username,
   'password' => md5($password)
   );
  $cek = $this->data_login->cek_login('admin',$where)->num_rows();
  if($cek > 0){

   $data_session = array(
    'nama' => $username,
    'status' => 'login',
    'role'  => 'admin'
    );

   $this->session->set_userdata($data_session);

   redirect(base_url('admin'));

  }elseif (!$cek>0) {
    $norek = $this->input->post('username');
    $pin = $this->input->post('password');
    if ($norek== '' && $pin == '' ) {
    $this->session->set_flashdata('salah','Isikan Form Dengan Benar!!!');
    $this->load->view('v_login');
  }
    $where = array(
   'norek_akun' => $pin,
   'pin' => $pin
   );
    $cek = $this->data_login->cek_login('akun',$where)->num_rows();
    if($cek > 0){

   $data_session = array(
    'nama' => $norek,
    'status' => 'login'
    );

      $this->session->set_userdata($data_session);

      redirect(base_url('nasabah')); 
    }else{
   redirect(base_url('login/salah'));
  }
 }
}

 function logout(){
  $this->session->sess_destroy();
  redirect(base_url('login'));
 }
}
?>