<?php 

class C_sampah extends CI_Controller{

 function __construct(){
  parent::__construct();
  $this->load->model('account_m');
  $this->load->model('sampah');

  if($this->session->userdata('status') != "login"){
   redirect(base_url("login"));
  }
 }

 function index(){
 	  $data['sampah'] = $this->sampah->sampah('sampah');
      // $data=array('data' => $this->sampah->sampah());
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('v_sampah',$data);
      $this->load->view('footer');
    }

 }
 ?>