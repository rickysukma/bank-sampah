<?php 
/**
 * 
 */
class Transaksi extends CI_Controller
{
	function __construct(){
  parent::__construct();
  $this->load->model('transaksi_m');
  $this->load->model('nasabah');
  $this->load->model('sampah');
  $this->load->model('account_m');

  if($this->session->userdata('status') != "login"){
   redirect(base_url("login"));
  }
 }

 function index(){
  $data['sampah'] = $this->sampah->sampah();
  $data['nasabah'] = $this->nasabah->nasabah();
  $data['transaksi'] = $this->transaksi_m->transaksi();
  $data['akun'] = $this->account_m->get_account('akun');
  $this->load->view('header');
  $this->load->view('sidebar');
  $this->load->view('transaksi',$data);
  $this->load->view('footer');
 }

 public function tes($akun,$saldo){
  $this->account_m->add_saldo($akun,$saldo);
   if($ex) {
          $this->session->set_flashdata('notif','Transaksi Sukses');
        redirect('transaksi');
        } else {
          $this->session->set_flashdata('gagal','Transaksi gagal');
        // redirect('transaksi');
        }
 }

 function tambah_transaksi(){
  $data = array(
            'tgl_transaksi'    => date("Y-m-d"),
            'norek_akun' => $this->input->post('norek_akun'),
            'nama_sampah'  => $this->input->post('sampah'),
            'jumlah'   => $this->input->post('jumlah'),
            'total' => $this->input->post('total')
        );
      $ex = $this->transaksi_m->tambah($data);
        if ($ex) {
          $akun = $this->input->post('norek_akun');
          $total = $this->input->post('total');
          $this->account_m->add_saldo($akun,$total);
          $this->session->set_flashdata('notif','Transaksi Sukses');
        redirect('transaksi');
        } else {
          $this->session->set_flashdata('gagal','Transaksi gagal');
        redirect('transaksi');
        }

    }

  }
 ?>