<?php
class Admin extends CI_Controller{

 function __construct(){
  parent::__construct();
  $this->load->model('nasabah');
  $this->load->model('sampah');
  $this->load->model('account_m');

  if($this->session->userdata('status') != "login"){
   redirect(base_url("login"));
  }
 }

 function index(){
  $data['akun'] = $this->account_m->get_account();
  $data['sampah'] = $this->sampah->sampah('sampah');
  $this->load->view('header');
  $this->load->view('sidebar');
  $this->load->view('dashboard',$data);
  $this->load->view('footer');
 }

 function sampah(){

 }

 function nasabah(){
  // $cek = $this->nasabah->getData('nasabah', 'id_nasabah', 'DESC')->row();
  $row = $this->db->query('SELECT MAX(id_nasabah) AS `maxid` FROM `nasabah`')->row();
      if ($row) {
          $maxid = $row->maxid; 
      }
      $num = $maxid + 1;
      $num_padded = sprintf("%06d", $num);
    
    // if(empty($id)){
    //   $noNew = '00001'; 
    // }else{
    //   $noOld = (int) substr($query->id_nasabah, 8, 5);
    //   $noOld++;
    //   $noNew = sprintf('%05s', $noOld);
    // }
      $date = date("m");
      $datestr = sprintf("%02d",$date);
      $plus = $maxid + 1 + $datestr;
      $noRekening = $plus.'BS'.$num_padded;
  $data['norek'] = $noRekening;   
  $data['nasabah'] = $this->nasabah->nasabah('nasabah');
  // $data=array(
  // 'data' => $this->nasabah->nasabah('nasabah')
  // );
  $this->load->view('header');
  $this->load->view('sidebar');
  $this->load->view('nasabah/home',$data);
  $this->load->view('footer');
 }

  
  // NASABAH FUNCTION
 function nasabah_detail($id){

    if ($id == NULL) {
      redirect('admin/nasabah');
    }else {

    $decodeid = base64_decode($id);
    $data=array(
    'data' => $this->nasabah->nasabah_join_akun($decodeid));
    $this->load->view('header');
    $this->load->view('sidebar');
    $this->load->view('nasabah/detail',$data);
    $this->load->view('footer');
    }
   }

 function add_nasabah(){
 	
  $data = array(
            'nama'		=> $this->input->post('nama'),
            'norek_akun' => $this->input->post('norek'),
            'alamat'	=> $this->input->post('alamat'),
            'pin'		=> $this->input->post('pin'),
            'no_telp'	=> $this->input->post('no_telp')
        );
        $ex = $this->nasabah->tambah($data);
        if ($ex) {
          $akun = array(
            'norek_akun' => $this->input->post('norek'),
            'date_opened' => date("Y-m-d"),
            'balance' => '0'

          );
          $this->account_m->tambah($akun);
        	$this->session->set_flashdata('notif','Berhasil Ditambahkan');
        redirect('admin/nasabah');
        } else {
        	$this->session->set_flashdata('gagal','Gagal Ditambahkan');
        redirect('admin/nasabah');
        }
    }

     public function del_nasabah($id){
    	$where = $id;
    	$ex = $this->nasabah->hapus($where);
    	if (!$ex) {
    		$this->session->set_flashdata('notif','Berhasil Dihapus');
        $this->del_akun($where);
    	} else {
    		$this->session->set_flashdata('gagal','Gagal Dihapus');
    	}
    	redirect('admin/nasabah');

    }
    public function del_akun($id){
      $where = $id;
      $ex = $this->account_m->hapus($where);

    }

    function update_nasabah($id_nasabah){
    $where = $id_nasabah;
        $data = array('id_nasabah' => $id_nasabah,
        'nama' => $this->input->post('nama'),
        'alamat' => $this->input->post('alamat'),
        'no_telp' => $this->input->post('no_telp'),
        'pin' => $this->input->post('pin'),
        'norek_akun' => $this->input->post('norek')

      );
        $ex = $this->nasabah->update($data,$where);
        if ($ex) {
          $this->session->set_flashdata('notif','Berhasil Diubahn');
        redirect('admin/nasabah');
        } else {
          $this->session->set_flashdata('notif','Gagal Diubah');
        redirect('admin/nasabah');
        }
    }


    //SAMPAH FUNTION
    function add_sampah(){
    $data = array(
            'nama_sampah'   => $this->input->post('nama_sampah'),
            'tipe_sampah' => $this->input->post('tipe_sampah'),
            'harga_sampah'    => $this->input->post('harga_sampah')
        );
        $ex = $this->sampah->tambah($data);
        if ($ex) {
          $this->session->set_flashdata('notif','Berhasil Ditambahkan');
        redirect('c_sampah');
        } else {
          $this->session->set_flashdata('notif','Gagal Ditambahkan');
        redirect('admin');
        }
    }

    function del_sampah($id){
      $where = $id;
      $ex = $this->sampah->hapus($where);
      if (!$ex) {
        $this->session->set_flashdata('notif','Berhasil Dihapus');
      } else {
        $this->session->set_flashdata('notif','Gagal Dihapus');
      }
      redirect('c_sampah');

    }

    function update_sampah($id_sampah){
    $where = $id_sampah;
        $data = array('id_sampah' => $id_sampah,
        'nama_sampah'   => $this->input->post('nama_sampah'),
            'tipe_sampah' => $this->input->post('tipe_sampah'),
            'harga_sampah'    => $this->input->post('harga_sampah')

      );
        $ex = $this->sampah->update($data,$where);
        if ($ex) {
          $this->session->set_flashdata('notif','Berhasil Diubah');
        redirect('c_sampah');
        } else {
          $this->session->set_flashdata('notif','Gagal Diubah');
        redirect('c_sampah');
        }
    }


 }



?>